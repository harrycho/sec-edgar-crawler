

* Accessing Edgar Data
    * https://www.sec.gov/edgar/searchedgar/accessing-edgar-data.htm

* Root link
    * https://www.sec.gov/Archives/edgar/full-index/
* Year
    * 1993-2018
* Quarter
    * QTR1-QTR4

* Contents
    * company — sorted by company name
    * form — sorted by form type
    * master — sorted by CIK number
    * XBRL — list of submissions containing XBRL financial files, sorted by CIK number; 
    these include Voluntary Filer Program submissions
    
####Link format
`<root_link>/<year>/<quarter>`
