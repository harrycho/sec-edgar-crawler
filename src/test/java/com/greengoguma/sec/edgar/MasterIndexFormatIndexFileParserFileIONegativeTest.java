package com.greengoguma.sec.edgar;

import com.greengoguma.MasterIndexFormatIndexFileParser;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.io.File;
import java.io.FileNotFoundException;

import static org.hamcrest.core.Is.isA;

public class MasterIndexFormatIndexFileParserFileIONegativeTest {


    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void testValidateWithNonExistentFile() {
        expectedException.expect(RuntimeException.class);
        expectedException.expectCause(isA(FileNotFoundException.class));

        File file = new File("I don't exist");
        MasterIndexFormatIndexFileParser validator = new MasterIndexFormatIndexFileParser();
        validator.parse(file);
    }

}
