package com.greengoguma.sec.edgar;

import com.greengoguma.*;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.*;
import static org.hamcrest.Matchers.*;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.UUID;


public class MasterIndexFormatIndexFileParserTextInputTest {


    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void testParseWithOnlyHeaders() {
        expectedException.expect(isA(RuntimeException.class));
        expectedException.expectCause(isA(IndexFileParseException.class));

        File file = createAutoDeletedTextFile(
                "Description:           Master Index of EDGAR Dissemination Feed\n" +
                "Last Data Received:    March 31, 2003\n" +
                "Comments:              webmaster@sec.gov\n" +
                "Anonymous FTP:         ftp://ftp.sec.gov/edgar/\n" +
                "Cloud HTTP:            https://www.sec.gov/Archives/");
        MasterIndexFormatIndexFileParser parser = new MasterIndexFormatIndexFileParser();
        parser.parse(file);
    }

    @Test
    public void testParseWithValidText() {
        File file = createAutoDeletedTextFile(
                "Description:           Master Index of EDGAR Dissemination Feed\n" +
                "Last Data Received:    March 31, 2003\n" +
                "Comments:              webmaster@sec.gov\n" +
                "Anonymous FTP:         ftp://ftp.sec.gov/edgar/\n" +
                "Cloud HTTP:            https://www.sec.gov/Archives/\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "CIK|Company Name|Form Type|Date Filed|Filename\n" +
                "--------------------------------------------------------------------------------\n" +
                "1000015|META GROUP INC|10-K|2003-03-28|edgar/data/1000015/0001047469-03-010821.txt\n" +
                "1000015|META GROUP INC|SC 13G/A|2003-02-04|edgar/data/1000015/0000080255-03-000161.txt\n" +
                "1000015|META GROUP INC|SC 13G/A|2003-02-11|edgar/data/1000015/0000919574-03-000244.txt");
        MasterIndexFormatIndexFileParser parser = new MasterIndexFormatIndexFileParser();
        IndexFile indexFile = parser.parse(file);
        RowReader<IndexTableRow> rowReader = indexFile.getReader();
        assertNotNull(rowReader.next());
        assertNotNull(rowReader.next());
        assertNotNull(rowReader.next());
        assertNull(rowReader.next());
    }


    private File createAutoDeletedTextFile(String text) {
        try {
            File file = File.createTempFile(MasterIndexFormatIndexFileParserTextInputTest.class.getName(), UUID.randomUUID().toString());
            file.deleteOnExit();
            PrintWriter writer = new PrintWriter(file);
            writer.write(text);
            writer.flush();
            return file;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}
