package com.greengoguma.sec.edgar;

import org.junit.Test;

import java.net.MalformedURLException;
import java.net.URISyntaxException;

public class EdgarIndexUrlBuilderNegativeTest {

    @Test(expected = IllegalArgumentException.class)
    public void testBuildWithUnsetYear() throws MalformedURLException, URISyntaxException {
        createReadyToBuildBuilder().setYear(null).build();
    }

    @Test(expected = IllegalArgumentException.class)
    public void testBuildWithUnsetFiledQuarter() throws MalformedURLException, URISyntaxException {
        createReadyToBuildBuilder().setFiledQuarter(null).build();
    }

    @Test(expected = IllegalArgumentException.class)
    public void testBuildWithUnsetIndexFileType() throws MalformedURLException, URISyntaxException {
        createReadyToBuildBuilder().setIndexFileType(null).build();
    }

    @Test(expected = IllegalArgumentException.class)
    public void testBuildWithIllegallyFormattedYear() {
        createReadyToBuildBuilder().setYear("abcdef");
    }

    private EdgarIndexUrlBuilder createReadyToBuildBuilder() {
        EdgarIndexUrlBuilder builder = new EdgarIndexUrlBuilder();
        builder.setYear("1234").setFiledQuarter(FiledQuarter.QTR2).setIndexFileType(IndexFileType.MASTER);
        return builder;
    }
}
