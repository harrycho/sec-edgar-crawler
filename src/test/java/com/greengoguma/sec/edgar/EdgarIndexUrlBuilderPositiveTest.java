package com.greengoguma.sec.edgar;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.Collection;

@RunWith(Parameterized.class)
public class EdgarIndexUrlBuilderPositiveTest {
    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] {
                { "2018", FiledQuarter.QTR1, IndexFileType.CRAWLER,
                        "https://www.sec.gov/Archives/edgar/full-index/2018/QTR1/crawler.idx"},
                { "2019", FiledQuarter.QTR2, IndexFileType.MASTER,
                        "https://www.sec.gov/Archives/edgar/full-index/2019/QTR2/master.idx"},
                { "2080", FiledQuarter.QTR3, IndexFileType.FORM,
                        "https://www.sec.gov/Archives/edgar/full-index/2080/QTR3/form.idx"},
                { "1900", FiledQuarter.QTR4, IndexFileType.CRAWLER,
                        "https://www.sec.gov/Archives/edgar/full-index/1900/QTR4/crawler.idx"},
        });
    }

    @Parameterized.Parameter(0)
    public String inputYear;

    @Parameterized.Parameter(1)
    public FiledQuarter inputFiledQuarter;

    @Parameterized.Parameter(2)
    public IndexFileType inputIndexFileType;

    @Parameterized.Parameter(3)
    public String expectedUrlString;

    @Test
    public void testBuild() throws MalformedURLException, URISyntaxException {
        EdgarIndexUrlBuilder builder = new EdgarIndexUrlBuilder();
        builder.setYear(inputYear).setFiledQuarter(inputFiledQuarter).setIndexFileType(inputIndexFileType);
        Assert.assertEquals(expectedUrlString, builder.build().toString());
    }
}
