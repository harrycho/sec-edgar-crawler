package com.greengoguma;

import com.greengoguma.sec.edgar.EdgarIndexUrlBuilder;
import com.greengoguma.sec.edgar.FiledQuarter;
import com.greengoguma.sec.edgar.IndexFileType;
import org.apache.commons.io.IOUtils;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

public class IndexFileDownloader {
    private static final String RESOURCE_DIR = "C:\\Users\\Harry Cho\\Programming\\git\\sec-web-crawler\\src\\main\\resources";

    public static void main(String [] args) throws IOException, URISyntaxException {
        CloseableHttpClient client = HttpClients.createDefault();
        List<String> years = generateYears(2018, 2018);
        EdgarIndexUrlBuilder builder = new EdgarIndexUrlBuilder();
        for(String year : years) {
            builder.setYear(year);
            for(FiledQuarter filedQuarter : FiledQuarter.values()) {
                builder.setFiledQuarter(filedQuarter);
                builder.setIndexFileType(IndexFileType.MASTER);
                URI indexFileLocation = builder.build();
                HttpGet getRequest = new HttpGet(indexFileLocation);

                String filename = year + "_" + filedQuarter.getUrlRepr() + "_" + IndexFileType.MASTER.getUrlRepr();
                File saveFile = new File(RESOURCE_DIR + "\\" + filename);
                saveFile.createNewFile();
                try (FileOutputStream fileOutputStream = new FileOutputStream(saveFile)) {
                    try (CloseableHttpResponse response = client.execute(getRequest)) {
                        IOUtils.copy(response.getEntity().getContent(), fileOutputStream);
                    }
                }
                System.out.println("");
            }
        }
    }

    /**
     *
     * @param begin Inclusive
     * @param end Exclusive
     * @return
     */
    private static List<String> generateYears(int begin, int end) {
        List<String> years = new ArrayList<>();
        for(int i = begin; i <= end; i++) {
            years.add(String.valueOf(i));
        }
        return years;
    }
}
