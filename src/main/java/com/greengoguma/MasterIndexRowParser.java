package com.greengoguma;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Locale;

/**
 *
 * CIK|Company Name|Form Type|Date Filed|Filename
 * --------------------------------------------------------------------------------
 * 1000015|META GROUP INC|10-K|2003-03-28|edgar/data/1000015/0001047469-03-010821.txt
 * 1000015|META GROUP INC|SC 13G/A|2003-02-04|edgar/data/1000015/0000080255-03-000161.txt
 * 1000015|META GROUP INC|SC 13G/A|2003-02-11|edgar/data/1000015/0000919574-03-000244.txt
 *
 */

public class MasterIndexRowParser implements RowParser<IndexTableRow> {

    private static final String SEPERATOR_REGEX = "\\|";
    private static final int COLUMN_COUNT = 5;

    @Override
    public IndexTableRow parse(String rawString) throws IndexFileParseException {
        String[] separated = rawString.split(SEPERATOR_REGEX);
        if (separated.length != COLUMN_COUNT) {
            throw new IndexFileParseException(
                    String.format("Line '%s' did not have '%d' items when separated by '%s'. Found '%s'.",
                            rawString, COLUMN_COUNT, SEPERATOR_REGEX, Arrays.toString(separated)));
        }
        return new IndexTableRow.IndexTableRowBuilder()
                .setCik(Integer.valueOf(separated[0]))
                .setCompanyName(separated[1])
                .setFormType(separated[2])
                .setDateFiled(parseDateFiled(separated[3]))
                .setFilename(separated[4]).build();
    }

    private Date parseDateFiled(String dateStr) throws IndexFileParseException {
        String dateFormat = "yyyy-MM-dd";
        DateFormat format = new SimpleDateFormat(dateFormat, Locale.ENGLISH);
        try {
            return format.parse(dateStr);
        } catch (ParseException e) {
            throw new IndexFileParseException(
                    String.format("Unable to parse '%s' into date using date format '%s'.", dateStr, dateFormat), e);
        }
    }
}
