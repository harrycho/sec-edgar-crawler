package com.greengoguma;

import java.util.regex.Pattern;

public class RegexMismatchException extends Exception {

    public RegexMismatchException(Pattern pattern, String inputLine) {
        super(String.format("Unable to match line '%s' using the pattern '%s'.", inputLine, pattern));
    }

    public RegexMismatchException(Pattern pattern, String inputLine, Throwable e) {
        super(String.format("Unable to match line '%s' using the pattern '%s'.", inputLine, pattern), e);
    }
}
