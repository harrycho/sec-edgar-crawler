package com.greengoguma.sec.edgar;

public enum IndexFileType {
    MASTER ("master.idx"),      // Sorted by CIK
    FORM ("form.idx"),          // Sorted by Form Type
    CRAWLER ("crawler.idx");    // Sorted by Company Name

    private String urlRepr;

    public String getUrlRepr() {
        return urlRepr;
    }

    IndexFileType(String urlRepr) {
        this.urlRepr = urlRepr;
    }
}
