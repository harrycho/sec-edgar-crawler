package com.greengoguma.sec.edgar;

public enum FiledQuarter {
    QTR1 ("QTR1"),
    QTR2 ("QTR2"),
    QTR3 ("QTR3"),
    QTR4 ("QTR4");

    private String urlRepr;

    public String getUrlRepr() {
        return urlRepr;
    }

    FiledQuarter(String urlRepr) {
        this.urlRepr = urlRepr;
    }
}