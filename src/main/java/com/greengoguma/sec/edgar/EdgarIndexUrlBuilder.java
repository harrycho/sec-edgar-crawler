package com.greengoguma.sec.edgar;

import org.apache.http.client.utils.URIBuilder;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.regex.Pattern;

/**
 *
 * Example)
 *      https://www.sec.gov/Archives/edgar/full-index/2015/QTR1/form.idx
 */

public class EdgarIndexUrlBuilder {

    private static final Pattern YEAR_REGEX = Pattern.compile("^[1-9]\\d{3,}$");

    private static final String EDGAR_INDEX_PATH = "/Archives/edgar/full-index";
    private static final String ADDRESS = "www.sec.gov";
    private static final String SCHEME = "https";

    /**
     * Required fields. Unset fields mean they are set to null or default values if primitive type.
     */
    private String year;
    private FiledQuarter filedQuarter;
    private IndexFileType indexFileType;

    public EdgarIndexUrlBuilder setYear(String year) {
        validateYear(year);
        this.year = year;
        return this;
    }
    public EdgarIndexUrlBuilder setFiledQuarter(FiledQuarter filedQuarter) {
        this.filedQuarter = filedQuarter;
        return this;
    }

    public EdgarIndexUrlBuilder setIndexFileType(IndexFileType indexFileType) {
        this.indexFileType = indexFileType;
        return this;
    }

    public URI build() throws URISyntaxException {
        checkRequiredFields();
        URIBuilder uriBuilder = new URIBuilder();
        uriBuilder.setScheme(SCHEME);
        uriBuilder.setHost(ADDRESS);
        uriBuilder.setPath(buildRelativeResourcePath());
        return uriBuilder.build();
    }

    private void checkRequiredFields() throws IllegalArgumentException {
        if (this.year == null || this.filedQuarter == null || this.indexFileType == null) {
            throw new IllegalArgumentException(String.format("Required fields are unset. " +
                    "Year=%s, FiledQuarter=%s, IndexFileTypes=%s", this.year, this.filedQuarter, this.indexFileType));
        }

}
    private void validateYear(String year) throws IllegalArgumentException {
        if(year != null && !YEAR_REGEX.matcher(year).matches()) {
            throw new IllegalArgumentException(String.format("Year '%s' is not a valid year.", year));
        }
    }


    private String buildRelativeResourcePath() {
        return EDGAR_INDEX_PATH +
                "/" + year +
                "/" + filedQuarter.getUrlRepr() +
                "/" + indexFileType.getUrlRepr();
    }
}
