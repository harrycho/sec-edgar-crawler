package com.greengoguma;

import java.util.Date;

/**
 *
 * CIK|Company Name|Form Type|Date Filed|Filename
 * --------------------------------------------------------------------------------
 * 1000015|META GROUP INC|10-K|2003-03-28|edgar/data/1000015/0001047469-03-010821.txt
 * 1000015|META GROUP INC|SC 13G/A|2003-02-04|edgar/data/1000015/0000080255-03-000161.txt
 * 1000015|META GROUP INC|SC 13G/A|2003-02-11|edgar/data/1000015/0000919574-03-000244.txt
 *
 */
public class IndexTableRow {
    private int cik;
    private String companyName;
    private String formType;
    private Date dateFiled;
    private String filename;

    private IndexTableRow(int cik, String companyName, String formType, Date dateFiled, String filename) {
        this.cik = cik;
        this.companyName = companyName;
        this.formType = formType;
        this.dateFiled = dateFiled;
        this.filename = filename;
    }

    @Override
    public String toString() {
        return "IndexTableRow{" +
                "cik=" + cik +
                ", companyName='" + companyName + '\'' +
                ", formType='" + formType + '\'' +
                ", dateFiled=" + dateFiled +
                ", filename='" + filename + '\'' +
                '}';
    }

    public static class IndexTableRowBuilder {
        private int cik;
        private String companyName;
        private String formType;
        private Date dateFiled;
        private String filename;

        public IndexTableRowBuilder setCik(int cik) {
            this.cik = cik;
            return this;
        }

        public IndexTableRowBuilder setCompanyName(String companyName) {
            this.companyName = companyName;
            return this;
        }

        public IndexTableRowBuilder setFormType(String formType) {
            this.formType = formType;
            return this;
        }

        public IndexTableRowBuilder setDateFiled(Date dateFiled) {
            this.dateFiled = dateFiled;
            return this;
        }

        public IndexTableRowBuilder setFilename(String filename) {
            this.filename = filename;
            return this;
        }

        public IndexTableRow build() {
            return new IndexTableRow(this.cik, this.companyName, this.formType, this.dateFiled, this.filename );
        }
    }

    public int getCik() {
        return cik;
    }

    public String getCompanyName() {
        return companyName;
    }

    public String getFormType() {
        return formType;
    }

    public Date getDateFiled() {
        return dateFiled;
    }

    public String getFilename() {
        return filename;
    }

}
