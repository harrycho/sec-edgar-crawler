package com.greengoguma;

import java.io.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * Description:           Master Index of EDGAR Dissemination Feed
 * Last Data Received:    March 31, 2003
 * Comments:              webmaster@sec.gov
 * Anonymous FTP:         ftp://ftp.sec.gov/edgar/
 * Cloud HTTP:            https://www.sec.gov/Archives/
 *
 *
 *
 *
 * CIK|Company Name|Form Type|Date Filed|Filename
 * --------------------------------------------------------------------------------
 * 1000015|META GROUP INC|10-K|2003-03-28|edgar/data/1000015/0001047469-03-010821.txt
 * 1000015|META GROUP INC|SC 13G/A|2003-02-04|edgar/data/1000015/0000080255-03-000161.txt
 * 1000015|META GROUP INC|SC 13G/A|2003-02-11|edgar/data/1000015/0000919574-03-000244.txt
 *
 *
 */


public class MasterIndexFormatIndexFileParser implements IndexFileParser {

    // Group# 1=Description
    private static Pattern DESCRIPTION_PATTERN = Pattern.compile("^Description:\\s*(.*)\\s*$");

    // Group# 1=Date (Format: March 31, 2003)
    private static Pattern LAST_DATA_RECEIVED_PATTERN =
            Pattern.compile("^Last Data Received:\\s+((\\w+)\\s+(\\d+),\\s+(\\d+))\\s*$");

    // Group# 1=Value
    private static Pattern COMMENTS_PATTERN = Pattern.compile("^Comments:\\s*(.*)\\s*$");

    // Group# 1=Value
    private static Pattern ANONYMOUS_FTP_PATTERN = Pattern.compile("^Anonymous FTP:\\s*(.*)\\s*$");

    // Group# 1=Value
    private static Pattern CLOUD_HTTP_PATTERN = Pattern.compile("^Cloud HTTP:\\s*(.*)\\s*$");

    private static Pattern COLUMN_NAMES_PATTERN = Pattern.compile("^CIK\\|Company Name\\|Form Type\\|Date Filed\\|Filename$");
    private static Pattern COLUMN_NAME_SEPARATER = Pattern.compile("^-+$");
    @Override
    public IndexFile parse(File masterIndexFile) {
        try {
            BufferedReader reader = new BufferedReader(new FileReader(masterIndexFile));

            String line = "";

            String description = parseTextValue(DESCRIPTION_PATTERN, reader.readLine());
            Date date = parseDateValue(LAST_DATA_RECEIVED_PATTERN, reader.readLine());
            String comment = parseTextValue(COMMENTS_PATTERN, reader.readLine());
            String anonymousFtp = parseTextValue(ANONYMOUS_FTP_PATTERN, reader.readLine());
            String cloudHttp = parseTextValue(CLOUD_HTTP_PATTERN, reader.readLine());

            expectWhiteLines(reader, 4);

            expectRegexMatch(COLUMN_NAMES_PATTERN, reader.readLine());
            expectRegexMatch(COLUMN_NAME_SEPARATER, reader.readLine());

            RowReader<IndexTableRow> rowRowReader = new ParsingRowReader<>(reader, new MasterIndexRowParser());
            return new IndexFile(description, date, comment, anonymousFtp, cloudHttp, rowRowReader);

        } catch (IOException e) {
            throw new RuntimeException(e);
        } catch (RegexMismatchException | IndexFileParseException e) {
            throw new RuntimeException(new IndexFileParseException(
                    String.format("Failed to parse file '%s'.", masterIndexFile.getAbsolutePath()), e));
        }
    }

    private String parseTextValue(Pattern pattern, String line) throws RegexMismatchException {
        if (line == null) {
            throw new RegexMismatchException(pattern, line);
        }
        Matcher matcher = pattern.matcher(line);
        if (matcher.matches()) {
            return matcher.group(1).trim();
        }
        throw new RegexMismatchException(pattern, line);
    }

    private Date parseDateValue(Pattern pattern, String line) throws RegexMismatchException {
        if (line == null) {
            throw new RegexMismatchException(pattern, line);
        }
        Matcher matcher = pattern.matcher(line);
        if (matcher.matches()) {
            String dateStr = matcher.group(1).trim();
            String dateFormat = "MMMM dd, yyyy";
            DateFormat format = new SimpleDateFormat(dateFormat, Locale.ENGLISH);
            try {
                return format.parse(dateStr);
            } catch (ParseException e) {
                throw new RegexMismatchException(pattern, line, e);
            }
        }
        throw new RegexMismatchException(pattern, line);
    }

    private void expectWhiteLines(BufferedReader reader, int count) throws IndexFileParseException {
        try {
            for(int i = 0; i < count; i++) {
                String line = reader.readLine();
                if (line == null || !line.trim().isEmpty()) {
                    throw new IndexFileParseException(
                            String.format("White lines are expected '%d' times. Found '%d'. Asserting line '%s'.",
                                    count, i, line));
                }
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private void expectRegexMatch(Pattern pattern, String line) throws RegexMismatchException {
        if (line == null) {
            throw new RegexMismatchException(pattern, line);
        }
        Matcher matcher = pattern.matcher(line);
        if (!matcher.matches()) {
            throw new RegexMismatchException(pattern, line);
        }
    }
}
