package com.greengoguma;

public class IndexFileParseException extends Exception {

    public IndexFileParseException(String message) {
        super(message);
    }

    public IndexFileParseException(String message, Throwable e) {
        super(message, e);
    }
}
