package com.greengoguma;

import java.io.File;
import java.util.Date;

/**
 *
 * Description:           Master Index of EDGAR Dissemination Feed
 * Last Data Received:    March 31, 2003
 * Comments:              webmaster@sec.gov
 * Anonymous FTP:         ftp://ftp.sec.gov/edgar/
 * Cloud HTTP:            https://www.sec.gov/Archives/
 *
 *
 *
 *
 * CIK|Company Name|Form Type|Date Filed|Filename
 * --------------------------------------------------------------------------------
 * 1000015|META GROUP INC|10-K|2003-03-28|edgar/data/1000015/0001047469-03-010821.txt
 * 1000015|META GROUP INC|SC 13G/A|2003-02-04|edgar/data/1000015/0000080255-03-000161.txt
 * 1000015|META GROUP INC|SC 13G/A|2003-02-11|edgar/data/1000015/0000919574-03-000244.txt
 *
 *
 */

public class IndexFile {

    private String description = "";
    private Date lastDataReceived;
    private String comments = "";
    private String anonymousFtp = "";
    private String cloudHttp = "";

    private RowReader<IndexTableRow> reader;

    public IndexFile(String description,
                     Date lastDataReceived,
                     String comments,
                     String anonymousFtp,
                     String cloudHttp,
                     RowReader<IndexTableRow> reader) {
        this.description = description;
        this.lastDataReceived = lastDataReceived;
        this.comments = comments;
        this.anonymousFtp = anonymousFtp;
        this.cloudHttp = cloudHttp;
        this.reader = reader;
    }

    public String getDescription() {
        return description;
    }

    public Date getLastDataReceived() {
        return lastDataReceived;
    }

    public String getComments() {
        return comments;
    }

    public String getAnonymousFtp() {
        return anonymousFtp;
    }

    public String getCloudHttp() {
        return cloudHttp;
    }

    public RowReader<IndexTableRow> getReader() {
        return reader;
    }
}
