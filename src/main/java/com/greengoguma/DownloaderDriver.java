package com.greengoguma;

import com.sun.org.apache.regexp.internal.RE;
import org.apache.commons.io.IOUtils;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.Buffer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class DownloaderDriver {
    private static final String RESOURCE_DIR = "C:\\Users\\Harry Cho\\Programming\\git\\sec-web-crawler\\src\\main\\resources";
    private static final String DOWNLOAD_DIR = RESOURCE_DIR + "\\download";

    private static List<CloseableHttpClient> clients = Collections.synchronizedList(new ArrayList<>());
    private static HttpClientPool pool = new HttpClientPool();

    private static BlockingQueue<String> messageQueue = new LinkedBlockingQueue<>(50);

    public static void main(String [] args ) throws IOException, InterruptedException {
        Set<String> forms = new HashSet<>();

        new File(DOWNLOAD_DIR).mkdir();
        long count = 0;
        List<Path> indexFiles;
        try (Stream<Path> paths = Files.walk(Paths.get(RESOURCE_DIR))) {
            indexFiles = paths.filter(Files::isRegularFile).filter(path -> path.endsWith("2018_QTR3_master.idx")).collect(Collectors.toList());
        }
        File file = indexFiles.get(0).toFile();
        MasterIndexFormatIndexFileParser parser = new MasterIndexFormatIndexFileParser();
        IndexFile indexFile = parser.parse(file);

        int downloaderCount = 20;
        ExecutorService executor = Executors.newFixedThreadPool(downloaderCount + 1);


        executor.submit(new MessageAdder(indexFile));
        for(int i = 0; i < downloaderCount; i++) {
            executor.submit(new DownloadThread(indexFile));
        }
        // This will make the executor accept no new threads
        // and finish all existing threads in the queue
        executor.shutdown();
        // Wait until all threads are finish
        executor.awaitTermination(40, TimeUnit.MINUTES);
        System.out.println("Finished all threads");


    }

    public static class HttpClientPool {

        private Queue<CloseableHttpClient> clients = new PriorityQueue<>();

        public synchronized CloseableHttpClient get() {
            if(clients.size() == 0) {
                return HttpClients.createDefault();
            } else {
                return clients.remove();
            }
        }

        public synchronized void put(CloseableHttpClient client) {
            clients.add(client);
        }
    }


    public static class MessageAdder implements Runnable {

        private IndexFile indexFile;

        public MessageAdder(IndexFile indexFile) {
            this.indexFile = indexFile;
        }

        @Override
        public void run() {
            try {

                RowReader<IndexTableRow> reader = indexFile.getReader();
                IndexTableRow row =  reader.next();
                while(row != null) {
                    row = reader.next();
                    System.out.println(String.format("Adding message %s...", row.getFilename()));
                    messageQueue.put(row.getFilename());
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public static class DownloadThread implements Runnable {

        private IndexFile indexFile;

        public DownloadThread(IndexFile indexFile) {
            this.indexFile = indexFile;
        }

        @Override
        public void run() {

            try {
                while(true) {
                    String filename = messageQueue.take();
                    CloseableHttpClient client = pool.get();
                    String savedFileName = filename.replace("/", "_");
                    String url = indexFile.getCloudHttp() +  filename;
                    HttpGet getRequest = new HttpGet(url);
                    File saveFile = new File(DOWNLOAD_DIR + "\\" + savedFileName);
                    saveFile.createNewFile();
                    System.out.println(String.format("Downloading %s...", url));
                    try (FileOutputStream fileOutputStream = new FileOutputStream(saveFile)) {
                        try (CloseableHttpResponse response = client.execute(getRequest)) {
                            IOUtils.copy(response.getEntity().getContent(), fileOutputStream);
                        }
                    }
                    pool.put(client);
                }
            } catch (InterruptedException | IOException e) {
                e.printStackTrace();
            }
        }
    }
}


