package com.greengoguma;

public interface RowReader<T> {

    T next();

}
