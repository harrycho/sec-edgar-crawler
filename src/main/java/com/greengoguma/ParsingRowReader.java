package com.greengoguma;

import java.io.BufferedReader;
import java.io.IOException;

public class ParsingRowReader<T> implements RowReader<T> {

    private RowParser<T> rowParser;
    private BufferedReader reader;

    public ParsingRowReader(BufferedReader reader, RowParser<T> rowParser) {
        this.rowParser = rowParser;
        this.reader = reader;
    }

    @Override
    public T next() {
        try {
            String line = reader.readLine();
            if(line == null) {
                return null;
            }
            return rowParser.parse(line);
        } catch (IOException | IndexFileParseException e) {
            throw new RuntimeException(e);
        }
    }
}
