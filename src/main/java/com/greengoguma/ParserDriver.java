package com.greengoguma;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ParserDriver {

    private static final String RESOURCE_DIR = "C:\\Users\\Harry Cho\\Programming\\git\\sec-web-crawler\\src\\main\\resources";

    public static void main(String [] args ) throws IOException {
        Set<String> forms = new HashSet<>();
        long count = 0;
        List<Path> indexFiles;
        try (Stream<Path> paths = Files.walk(Paths.get(RESOURCE_DIR))) {
            indexFiles = paths.filter(Files::isRegularFile).filter(path -> !path.endsWith("resources.7z")).collect(Collectors.toList());
        }
        for(Path indexFilePath : indexFiles) {
            File file = indexFilePath.toFile();
            MasterIndexFormatIndexFileParser parser = new MasterIndexFormatIndexFileParser();
            IndexFile indexFile = parser.parse(file);
            RowReader<IndexTableRow> reader = indexFile.getReader();
            IndexTableRow row =  reader.next();
            while(row != null) {
//                System.out.println(row);
                forms.add(row.getFormType());
                count++;
                row = reader.next();
            }
        }
        System.out.println(count);
        List<String> l = new ArrayList<>(forms);
        Collections.sort(l);
        for(String str : l) {
            System.out.println(str);
        }
    }
}
