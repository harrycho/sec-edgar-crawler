package com.greengoguma;

import java.io.File;

public interface IndexFileParser {

    IndexFile parse(File masterIndexFile);
}
