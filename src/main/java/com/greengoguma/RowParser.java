package com.greengoguma;

public interface RowParser<T> {
    T parse(String rawString) throws IndexFileParseException;
}
